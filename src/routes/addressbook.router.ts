import express               from 'express';
import AddressbookController from '../controllers/addressbook.controller';

const router = express.Router();

router.get('/addressbook', AddressbookController.getAllContacts);
router.get('/addressbook/:id', AddressbookController.getContactById);
router.put('/addressbook/:id', AddressbookController.editContact);
router.delete('/addressbook/:id', AddressbookController.deleteContact);
router.post('/addressbook', AddressbookController.addContact);

export = router;
