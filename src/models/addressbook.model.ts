import { Schema, model } from 'mongoose';

let schema: Schema = new Schema({
    firstName: String,
    lastName : String,
    email    : String,
    country  : String
});

export default model('Contact', schema);
