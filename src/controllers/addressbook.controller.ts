import { Request, Response, NextFunction } from 'express';
import AddressbookModel                    from '../models/addressbook.model';

export default class AddressbookController {

    public static async getAllContacts(req: Request, res: Response, next: NextFunction) {
        try {
            let result = await AddressbookModel.find().exec();

            res.send({
                message: 'Successfully retrieved all contacts!',
                status : 200,
                result : result
            });
        } catch (err) {
            res.send({
                message: 'Unable to get the contacts.',
                status : 400,
                err    : err
            });
        }
    }

    public static async getContactById(req: Request, res: Response, next: NextFunction) {
        try {
            let result = await AddressbookModel.findOne({_id: req.params.id}).exec();

            if (result) {
                res.send({
                    message: 'Contact found!',
                    status : 200,
                    result : result
                });
            } else {
                res.send({
                    message: 'Contact not found.',
                    status : 404,
                    result : result
                });
            }
        } catch (err) {
            res.send({
                message: 'Trying to find the contact failed.',
                status : 400,
                err    : err
            });
        }
    }

    public static async editContact(req: Request, res: Response, next: NextFunction) {
        try {
            let result = await AddressbookModel.findOneAndUpdate({_id: req.params.id}, req.body, {returnOriginal: false}).exec();

            res.send({
                message: 'Successfully modified the contact!',
                status : 200,
                result : result
            });
        } catch (err) {
            res.send({
                message: 'Unable to edit the contact.',
                status : 400,
                err    : err
            });
        }
    }

    public static async deleteContact(req: Request, res: Response, next: NextFunction) {
        try {
            let result = await AddressbookModel.findOne({_id: req.params.id}).remove().exec();

            res.send({
                message: 'Successfully removed the contact!',
                status : 200,
                result : result
            });
        } catch (err) {
            res.send({
                message: 'Unable to delete the contact.',
                status : 400,
                err    : err
            });
        }
    }

    public static async addContact(req: Request, res: Response, next: NextFunction) {
        try {
            let newContact = new AddressbookModel(req.body);

            await newContact.save();

            res.send({
                message: 'New contact created!',
                status : 200,
                result : newContact
            });
        } catch (err) {
            res.send({
                message: 'Unable to create the contact.',
                status : 400,
                err    : err
            });
        }
    }
}
